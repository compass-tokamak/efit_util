#!/usr/bin/env bash

python ../scripts/efit_inputs.py ../examples/92436_chain1 ./new --type target
cmp --silent --ignore-initial=1000 new/input.nc valid/input.nc || echo "the file produced by scripts/efit_inputs.py is different from valid/input.nc"
