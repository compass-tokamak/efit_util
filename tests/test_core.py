import unittest
import efit_util as eutil
import os

input_path = './orig'
run_path = './new'
idam_path = None # this is true for freia
init_cmd = 'source efit++-setup efit_uda' # true for freia

class TestCore(unittest.TestCase):
    
    def test_from_path(self):        
        efitObj = eutil.EfitIO.frompath(run_path, input_path)
        return efitObj
        
    
    def test_read_nc(self):
        efitObj = self.test_from_path()
        for measurement_type in ['target', 'computed']:
            efitObj.read_nc_output(output_file=os.path.join(input_path, 'efitOut.nc'), sig_type=measurement_type)
        return efitObj
        
    def test_save_all_inputs(self):
        efitObj = self.test_read_nc()
        efitObj.save_all_inputs(idam_path=idam_path)
        return efitObj

    def test_run(self):
        efitObj = self.test_save_all_inputs()
        efitObj.run(init_cmd)
        return efitObj

if __name__ == '__main__':
    unittest.main()
