from setuptools import setup, find_packages

setup(
    name='efit_util',
    version='0.1.0',
    author='Vit Skvara',
    author_email='skvara@ipp.cas.cz',
    packages=find_packages(),
#    scripts=['scripts/efit_inputs.py','scripts/mc_experiment.py','scripts/analyze_mc_experiment.py'],
    url='https://bitbucket.org/compass-tokamak/efit_util/src/master/',
    description='EFIT++ utilities',
    long_description=open('README').read(),
    install_requires=[
        "numpy >= 1.11.0",
        "lxml >= 4.1.0",
    ]
)
