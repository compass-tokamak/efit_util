#!/usr/bin/env bash

# this will create new run directory with efit inputs extracted from the original
# one and run EFIT++ there as well
# requires python3 - on Freia, call 'module unload python/2.7.5 && module load python/3.5'
INPATH=./92436_chain1
OUTPATH=./92436_chain1_new
python ../efit_util/scripts/efit_inputs.py $INPATH $OUTPATH --type target --run
