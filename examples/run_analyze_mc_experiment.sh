#!/usr/bin/env bash

# this will produce anylsis graphs of the MC experiment
# requires python3 - on Freia, call 'module unload python/2.7.5 && module load python/3.5'
EXPPATH=./92436_mc_experiment/computed_N-10_relerr-0.05/
python ../efit_util/scripts/analyze_mc_experiment.py $EXPPATH --vars /output/globalParameters/q95 --q0
