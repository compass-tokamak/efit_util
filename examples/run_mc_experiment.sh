#!/usr/bin/env bash

# this will run an MC EFIT++ experiment based on teh data in the INPATH
# requires python3 - on Freia, call 'module unload python/2.7.5 && module load python/3.5'
INPATH=./92436_chain1
OUTPATH=./92436_mc_experiment
NITER=10
python ../efit_util/scripts/mc_experiment.py $INPATH $OUTPATH $NITER --type computed --relerr 0.05
