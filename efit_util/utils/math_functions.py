import numpy as np
from scipy.stats import t

def confidence_interval(x, c):
	""" Returns the confidence interval, mean, standard deviation 
	and the width of the confidence interval fro data x and confidence level c.
	"""
	N = len(x)
	mx = np.mean(x)
	sdx = np.std(x)

	alpha = (1-c)/2.0
	df = N-1
	tstar = - t.ppf(alpha,df)
	ci = tstar*sdx/np.sqrt(N) 
	return [mx-ci, mx+ci], mx, sdx, ci

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def circular_angle(v):
	""" Return the angle of the vector as if it was projected on a circle."""
	a = angle_between([1,0], v)
	if v[1] < 0:
		a = 2*np.pi - a
	return a


def bound_angles(bound):
	""" Computes circular angles for all point in a boundary."""
	axis = [np.mean(bound["R"]), np.mean(bound["Z"])]
	return np.array([circular_angle([r-axis[0], z-axis[1]]) for (r,z) in zip(bound["R"], bound["Z"])])

