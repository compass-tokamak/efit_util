from efit_util.utils import *
import os
import netCDF4
import json
import numpy
import copy

def load_experiment_info(path):
    """ returns shot number and time slices from an MC experiment path """
    # loads the first available efitOut.nc and retrieves the time vector and shot number
    dirs = os.listdir(path)
    for dir in dirs:
        i = int(dir)
        dir = os.path.join(path, dir)
        fnc = os.path.join(dir,'efitOut.nc')
        if os.path.isfile(fnc):
            # load the output nc file
            nc = netCDF4.Dataset(os.path.join(dir,'efitOut.nc'),'r')
            t = nc['time'][:].data
            shotno = nc.pulseNumber
            limiter = nc["/input/limiter/rValues"][:].data, nc["/input/limiter/zValues"][:].data
            nc.close()
            return shotno, t, limiter

def load_experiments(path, variable_adresses, idata):
    """ returns a list of all experiments in a given path, extracting variables 
        given by variable adresses from the efitOut.nc file  """
    # make a list of all dirs
    dirs = os.listdir(path)
    
    data = dict()
    for dir in dirs:
        i = int(dir)
        dir = os.path.join(path, dir)
        fnc = os.path.join(dir,'efitOut.nc')
        if os.path.isfile(fnc):
            # load the output nc file
            nc = netCDF4.Dataset(os.path.join(dir,'efitOut.nc'),'r')
            for adress in variable_adresses:
                x = None
                var_name = adress.split('/')[-1]
                # check if the variable of a given adress exists at all
                try:
                    x = nc[adress][idata]
                except e:
                    print(adress + " not found in the efitOut.nc file")
                # if it does, write it in the dict
                if x is not None:
                    if data.get(var_name) is None:
                        data[var_name] = []
                    data[var_name].append(x)          
            nc.close()
        else:
           print('{} not present'.format(fnc))
    return data

def dict_ndarray_to_list(d):
    """ for subdictionaries in a dictionary, whis will convert np.arrays to lists 
        so they can be serialized
     """
    _d = copy.deepcopy(d)
    for key in _d.keys():
        item = _d[key]
        if type(item)==dict:
            for ikey in item.keys():
                data = item[ikey]
                if type(data)==np.ndarray:
                    item[ikey]=data.tolist()
    return _d

def save_json(result, file):
    """ dumps a nested dictionary with analysis results into a json file """
    _result = dict_ndarray_to_list(result)
    with open(file, 'w') as f:
        json.dump(_result, f, indent=4)
