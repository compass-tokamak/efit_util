from efit_util.utils import *
import matplotlib.pyplot as plt
import numpy as np
import os

def boxplot(x, c = 0.95, title="", ylabel=""):
    """ create a boxplot for the MC experiment analysis, currently outdated """
    # boxplot
    plt.figure()
    plt.boxplot(x, showmeans=True, meanline=True, notch=True)
    plt.title(title)
    # confidence interval
    mu_ci, mu, sd, ci = confidence_interval(x, c)
    md = np.median(x)
    plt.plot([1-0.1, 1+.1], np.ones(2)*md-ci, c= "b", label = "median confidence interval")
    plt.plot([1-0.1, 1+.1], np.ones(2)*md+ci, c= "b", label = "median confidence interval")
    plt.scatter(np.ones(2), mu_ci, c= "k", marker = "x", label = "confidence interval")
    plt.scatter(np.ones(2), [mu-2*sd, mu+2*sd], c= "r", marker = "x", label = "2sigma")
    plt.scatter(np.ones(2), [mu-sd, mu+sd], c= "g", marker = "x", label = "sigma")
    plt.ylabel(ylabel)
    plt.legend()

def plot_lines(ax, y, pos=1, l=0.1, label=None, **kwargs):
    """ plot two lines at y[1] and y[0] """
    assert len(y)==2
    ax.plot([pos-l, pos+l], np.ones(2)*y[0], **kwargs)
    if label is None:
        ax.plot([pos-l, pos+l], np.ones(2)*y[1], **kwargs)
    else:
        ax.plot([pos-l, pos+l], np.ones(2)*y[1], label=label, **kwargs)

def fill(ax, y, pos=1, l=0.1, **kwargs):
    """ fill the space between y[0] and y[1] """
    assert len(y)==2
    ax.fill_between([pos-l, pos+l], np.ones(2)*y[0], np.ones(2)*y[1], **kwargs)
    
def dual_boxplot_old(x, c=0.95, title="", ylabel=""):
    """ create a boxplot for the MC experiment analysis """
    xl = [0.85, 1.15]
    f,axs = plt.subplots(1,2,sharey = True)
    f.suptitle(title)
    ax = axs[0]
    ax.boxplot(x, notch=True)
    ax.set_title("boxplot") # median, 1st and 3rd quartile, 95% confidence interval, minimum and maximum
    ax.set_ylabel(ylabel)
    ax.set_xlim(xl)

    ax = axs[1]
    ax.set_title("error bars")
    mu_ci, mu, sd, ci = confidence_interval(x, c)
    ax.plot([1-0.1, 1+0.1], np.ones(2)*mu, c ="k", label="mean")
    plot_lines(ax, mu_ci, label="{:.1f}% confidence interval = {:.2f}".format(c*100, ci), 
        c="g")
    fill(ax, [mu, mu+ci], facecolor="g", alpha=0.4)
    fill(ax, [mu, mu-ci], facecolor="g", alpha=0.4)
    plot_lines(ax, [mu-2*sd, mu+2*sd], label="2$\sigma$ = {:.2f}".format(2*sd), 
        c="y")
    fill(ax, [mu+ci, mu+2*sd], facecolor="y", alpha=0.2)
    fill(ax, [mu-ci, mu-2*sd], facecolor="y", alpha=0.2)
    ax.set_xlim(xl)
    ax.legend()
    
    f.subplots_adjust(wspace=0)
    
def dual_boxplot(x, c=0.95, nsigmas=2, title="", ylabel=""):
    """ create a boxplot for the MC experiment analysis """
    xl = [0.85, 1.15]
    f,axs = plt.subplots(1,2,sharey = False)
    f.suptitle(title)
    ax = axs[0]
    ax.boxplot(x, notch=True)
    ax.set_title("boxplot") # median, 1st and 3rd quartile, 95% confidence interval, minimum and maximum
    ax.set_ylabel(ylabel)
    ax.set_xlim(xl)

    ax = axs[1]
    ax.set_title("error bars")
    mu_ci, mu, sd, ci = confidence_interval(x, c)
    # plot the bar
    lb = 0.1
    ax.plot([1-lb, 1+lb], np.ones(2)*mu, c ="b", label="mean")
    fill(ax, [0, mu], l=lb, facecolor="b", alpha=0.3)
    # plot the error bars
    le = 0.05
    plot_lines(ax, [mu-nsigmas*sd, mu+nsigmas*sd],  l=le, 
        label="{}$\sigma$ = {:.2f}".format(nsigmas, nsigmas*sd), 
        c="y")
    ax.plot([1, 1], [mu-nsigmas*sd, mu+nsigmas*sd], c="y")
    plot_lines(ax, mu_ci, l=le,
        label="{:.1f}% confidence interval = {:.2f}".format(c*100, ci), c="g")
    ax.plot([1, 1], mu_ci, c="g")
    # set axes
    ax.set_xlim(xl)
    yl = np.array(ax.get_ylim())
    yl[0] = 0  
    yl[1] *= 1.1  
    ax.set_ylim(yl)
    ax.yaxis.tick_right()
    ax.legend()
    
    f.subplots_adjust(wspace=0)

    return mu, ci, sd

def scalar_plots(data, info, c, nsigmas, savepath=''):
    """ create boxplots from a data dictionary """
    var_keys = list(filter(lambda x: x!='pulse_number',[x for x in data.keys()]))
    shot_no = info['pulse_number']
    t = info['t']
    # return variable
    res = {}
    # now make boxplots
    for var in var_keys:
        x = data.get(var)
        if x is None:
            continue
        Nexp = len(x)
        if type(x[0]) is np.ndarray:
            print(var + " is an array, cannot make a boxplot")
        else:    
            # otherwise call the boxplot function
            ts = 'shot {}, t = {}, {} boxplot, {} experiments'.format(
                shot_no, t, var, Nexp, c)
            res[var] = {}
            res[var]['mean'], res[var]['ci'], res[var]['sd'] = dual_boxplot(x, c=c, nsigmas=nsigmas, title=ts, ylabel=var)
            # save the figure
            if savepath != '':
                plt.savefig(os.path.join(savepath, "t-{}_{}_c-{}_nsigma-{}_distribution.png".format(
                    t, var, c, nsigmas)))

    return res
        
def bounds_overlay(data, info, savepath='', limiter=None):
    """ plot all boundaries in data """
    bounds = data.get('boundaryCoords')
    if bounds is None:
        print("No boundaries given, exiting")
        return
    shot_no = info['pulse_number']
    t = info['t']
    Nexp = len(bounds)
    plt.figure()
    plt.title('{}, t = {}, boundaries, {} experiments'.format(shot_no, t,
        Nexp))
    if limiter:
        plt.plot(limiter[0], limiter[1], c="k", lw=1)
    for bound in bounds:
        plt.plot(bound['R'], bound['Z'],c='r',alpha=0.1)
        geometric_axis = (np.mean(bound['R']), np.mean(bound['Z']))
        plt.scatter(geometric_axis[0], geometric_axis[1],
            c = "r", marker = ".", s = 20, alpha=0.1)
    plt.xlabel("r [m]")
    plt.ylabel("z [m]")
    
    plt.legend(handles=[plt.Line2D([0], [0], color = 'w', marker='.', 
        markerfacecolor='r', label='geometric axis', markersize=10)])
    # save the figure
    if savepath != '':
        plt.savefig(os.path.join(savepath, "t-{}_boundaries_overlay.png".format(t)))

def bounds_process(data, c=0.95):
    """ compute mean boundary, confidence intervals and sd, also poloidal angles """
    bounds = data.get('boundaryCoords')
    if bounds is None:
        print('no boundaries available')
        return None, None, None
        
    # compute the r and z means
    N = bounds[0].size
    coords = ["R", "Z"]
    # a dictionary with r and z means, confidence interval bounds and standard deviations
    ci_vals = {} 
    
    # first, sort all the boudnaries by their poloidal angle - xpoint and nonxpoint boundaries 
    # are ordered differently
    for i, bound in enumerate(bounds):
        pol_angs = bound_angles(bound)
        inds = np.argsort(pol_angs)
        bounds[i]['R'] = bounds[i]['R'][inds]
        bounds[i]['Z'] = bounds[i]['Z'][inds] 
    
    # create the output structure
    for coord in coords:
        ci_vals[coord+"_mean"] = np.zeros(N)
        ci_vals[coord+"_pci"] = np.zeros(N)
        ci_vals[coord+"_mci"] = np.zeros(N)
        ci_vals[coord+"_sd"] = np.zeros(N)
        ci_vals[coord+"_ci"] = np.zeros(N)
        
    # fill it
    for i in range(N):
        for coord in coords:
            x = [bound[coord][i] for bound in bounds]
            res = confidence_interval(x,c)
            ci_vals[coord+"_mean"][i] = res[1]
            ci_vals[coord+"_pci"][i] = res[0][1]
            ci_vals[coord+"_mci"][i] = res[0][0]
            ci_vals[coord+"_sd"][i] = res[2]
            ci_vals[coord+"_ci"][i] = res[3]
        
    # compute the mean boundary and the poloidal angles
    mean_bound = np.array([(r,z) for (r,z) in 
        zip(ci_vals["R_mean"], ci_vals["Z_mean"])], dtype = np.dtype([('R', '<f8'), ('Z', '<f8')]))
    geometric_axis = np.array([np.mean(mean_bound["R"]), np.mean(mean_bound["Z"])])
    poloidal_angles = bound_angles(mean_bound)
    return mean_bound, ci_vals, poloidal_angles

def bounds_confidence_plot(data, info, c=0.95, savepath=''):
    """ plot the mean boundaries and confidence intervals """
    mean_bound, ci_vals, poloidal_angles = bounds_process(data, c=c)
    if mean_bound is None:
        print('no confidence intervals to plot')
        return
    shot_no = info['pulse_number']
    t = info['t']
    coords = ["R", "Z"]
    cols = {"R": "b", "Z": "r"}
    # this is used to store and return data
    res = {}
    for coord in coords:
        f,axs = plt.subplots(3,1,sharex = True)
        
        ax = axs[0]
        ax.set_title("{}, t = {}, {} mean and 95% confidence intervals".format(shot_no, t,
            coord))
        ax.plot(poloidal_angles, ci_vals[coord+"_mean"], c = cols[coord], label=coord+" [m]")
        ax.fill_between(poloidal_angles, ci_vals[coord+"_pci"], 
            ci_vals[coord+"_mci"], alpha = 0.2, facecolor = cols[coord])
        ax.set_ylabel("{} [m]".format(coord))
        ax.legend()

        ax = axs[1]
        ax.plot(poloidal_angles, ci_vals[coord+"_ci"], 
            c = cols[coord], label="{}% confidence interval, mean = {:.1e}m".format(c*100,
                np.mean(ci_vals[coord+"_ci"])))
        ax.set_ylabel("CI [m]")
        ax.legend()
        
        ax = axs[2]
        x = abs(ci_vals[coord+"_ci"]/ci_vals[coord+"_mean"])
        xinds = (x < 1.0) # here, cut off data that are created when mean is 0
        x = x[xinds]
        ax.plot(poloidal_angles[xinds], x, 
            c = cols[coord],
            label="{:.1f}% relative confidence interval, mean = {:.1e}".format(c*100,
                np.mean(x)))
        ax.set_xlabel("poloidal angle [rad]")
        ax.set_ylabel("CI/{} [-]".format(coord))
        ax.legend()

        # save the figure
        if savepath != '':
            plt.savefig(os.path.join(savepath, "t-{}_boundary-{}_c-{}_confidence_intervals.png".format(
                t, coord, c)))

        # return the data - mean, ci and sigma together with poloidal angle
        res[coord] = {}
        res[coord]['mean'] = ci_vals[coord+"_mean"]
        res[coord]['ci'] = ci_vals[coord+"_ci"]
        res[coord]['sd'] = ci_vals[coord+"_sd"]
        res[coord]['poloidal_angle'] = poloidal_angles
        
    return res
    
def bounds_sigma_plot(data, info, nsigmas = 2, c=0.95, savepath=''):
    """ plot mean boundaries and n sigma bands """
    mean_bound, ci_vals, poloidal_angles = bounds_process(data, c=c)
    if mean_bound is None:
        print('no sigma bands to plot')
        return
    shot_no = info['pulse_number']
    t = info['t']
    coords = ["R", "Z"]
    cols = {"R": "b", "Z": "r"}
    # this is used to store and return data
    res = {}
    for coord in coords:
        f,axs = plt.subplots(3,1,sharex = True)
        
        ax = axs[0]
        ax.set_title("{}, t = {}, {} mean and {}$\sigma$ bands".format(shot_no, 
            t, coord, nsigmas))
        ax.plot(poloidal_angles, ci_vals[coord+"_mean"], c = cols[coord], label=coord+" [m]")
        ax.fill_between(poloidal_angles, ci_vals[coord+"_mean"] + nsigmas*ci_vals[coord+"_sd"], 
            ci_vals[coord+"_mean"] - nsigmas*ci_vals[coord+"_sd"], alpha = 0.2, 
            facecolor = cols[coord])
        ax.set_ylabel("{} [m]".format(coord))
        ax.legend()

        ax = axs[1]
        ax.plot(poloidal_angles, nsigmas*ci_vals[coord+"_sd"], 
            c = cols[coord], label="{}$\sigma$, mean = {:.1e}m".format(nsigmas,
                nsigmas*np.mean(ci_vals[coord+"_sd"])))
        ax.set_ylabel("{}$\sigma$ [m]".format(nsigmas))
        ax.legend()
        
        ax = axs[2]
        x = abs(nsigmas*ci_vals[coord+"_sd"]/ci_vals[coord+"_mean"])
        xinds = (x < 1.0) # here, cut off data that are created when mean is 0
        x = x[xinds]
        ax.plot(poloidal_angles[xinds], x, 
            c = cols[coord],
            label="relative size of {}$\sigma$, mean = {:.1e}".format(nsigmas,
                np.mean(x)))
        ax.set_xlabel("poloidal angle [rad]")
        ax.set_ylabel("{}$\sigma$/{} [-]".format(nsigmas, coord))
        ax.legend()

        # save the figure
        if savepath != '':
            plt.savefig(os.path.join(savepath, "t-{}_boundary-{}_nsigma-{}_bands.png".format(
                t, coord, nsigmas)))
           # return the data - mean, ci and sigma together with poloidal angle
        res[coord] = {}
        res[coord]['mean'] = ci_vals[coord+"_mean"]
        res[coord]['ci'] = ci_vals[coord+"_ci"]
        res[coord]['sd'] = ci_vals[coord+"_sd"]
        res[coord]['poloidal_angle'] = poloidal_angles
        
    return res
    
