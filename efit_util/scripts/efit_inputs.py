#!/usr/bin/env python

# from a batch of EFIT++ input xmls and efitOut.nc file, create local running
# environment with data saved in input.nc file
# for details, run as python efit_inputs.py -h

import os 
import sys
import efit_util as eutil
import numpy as np
import netCDF4
import subprocess
import argparse

def __parse_args():
    # extract the arguments
    parser = argparse.ArgumentParser(description = 'Setup EFIT++ inputs from a given input path containing xml files and efitOut.nc file.')

    parser.add_argument('inpath', 
        help='directory containing the files')

    parser.add_argument('runpath', 
        help='desired output directory')

    parser.add_argument('--type', type = str,
        dest = 'measurement_type', 
        help='type of signals from the original efitOut.nc file to be used as input to the new run, default target', 
        choices = ['target', 'computed'],
        default = 'target')
        
    parser.add_argument('--run', action = 'store_true',
        help = 'run EFIT after creating the inputs')

    parser.add_argument('--recompute-response', action = 'store_true',
        help = 'recomputes the response functions')
        
    parser.add_argument('--xml-magnetics', action = 'store_true',
        dest='xml_magnetics',
        help='store magnetic signals in xml instead of .nc file')

    parser.add_argument('--tslices', type = float,
        dest='tslices',
        nargs="+",
        help='specify the timeslices that you want to use',
        default=None)

    args = parser.parse_args()
    
    return args
    
def efit_inputs(args):    
    # this will decide between abacus and freia
    host = subprocess.check_output(["echo $HOSTNAME"], shell=True).rstrip().decode()
    if host == 'abacus.tok.ipp.cas.cz':
        init_cmd = "module load efit++" # this is true for freia
        idam_path = None # this is true for freia
    else:
        init_cmd = "source efit++-setup efit_uda" # this is true for freia
        idam_path = None # this is true for freia
        
    # automatically extract the efit path
    efit_path = subprocess.check_output(["echo $EFIT_ROOT"], shell=True).rstrip()

    # init the object
    efitObj = eutil.EfitIO.frompath(args.runpath, args.inpath)

    # read the efitOut.nc file
    efitObj.read_nc_output(output_file=os.path.join(args.inpath, 'efitOut.nc'), sig_type=args.measurement_type)

    # read the xml input
    efitObj.read_xml_signals()
    
    # if needed, subsample the signals
    if not args.tslices==None:
        efitObj.limit_time_slices(np.array(args.tslices))

    # set and save the inputs
    efitObj.save_all_inputs(idam_path=idam_path, xml_magnetics = args.xml_magnetics,
        recompute_response = args.recompute_response)

    # eventually run it
    if args.run:
        efitObj.run(init_cmd)
        
    return True

if __name__=="__main__":
    args = __parse_args()
    
    # run it
    efit_inputs(args)
		
