import netCDF4
import sys
import os
import argparse
import datetime

def __parse_args():
    # extract the arguments
    parser = argparse.ArgumentParser(description = 'Recover and store signals from an EFIT++ output file.')

    parser.add_argument('inpath', 
        help='directory containing a EFIT++ run(s)')
    
    parser.add_argument('vars',
        nargs='+',
        help='specify the efitOut.nc variable adress that you want to extract.')
    
    parser.add_argument('--outpath',
        help="specify outpath",
        default=None)
    
    parser.add_argument('-r', action='store_true',
        help='recursive')
        
    parser.add_argument('-v', action = 'store_true',
        help = 'control verbosity')
    
    parser.add_argument("--delete-old", action='store_true',
        help = 'delete old efitOut.nc files',
        dest="delete_old")
    
    args = parser.parse_args()

    return args
    
    
def extract_and_save(args):
    inpath=args.inpath
    if args.r:
        contents=[x for x in map((lambda y: os.path.join(inpath,y)), os.listdir(inpath))]
    else:
        contents=[inpath]
        
    for c in contents:
        infile = os.path.join(c,"efitOut.nc")
        print(infile)
        print(os.listdir(c))
        if not "efitOut.nc" in os.listdir(c):
            print("no efitOut.nc present in")
            print(c)
            continue
        # load the in file and setup the outfile
        efitin = netCDF4.Dataset(infile)
        if (not args.outpath==None):
            if args.r:
                c = os.path.join(args.outpath, c.split("/")[-1])   
            else:
                c = args.outpath
                
        os.makedirs(c,exist_ok=True)
        print(c)
        efitout = netCDF4.Dataset(os.path.join(c,"efitOut_extracted.nc"),'w', format='NETCDF4')
        
        # setup the rest
        attrs = {u'Conventions': u'Fusion-1.0, MAST-1.0',
                 u'class': u'raw data',
                 u'comment': u'',
                 u'date': datetime.date.today().strftime('%Y-%m-%d'),
                 u'shot': u'0',
                 u'software': u'datac-123, netcdf-4.3, hdf5-1.8.3',
                 u'title': u''}
        efitout.setncatts(attrs)

        # first create the time dimension and variable
        times = efitin["time"]
        efitout.createDimension('time', len(times))
        times_data = efitout.createVariable('time', 'f8', ('time', ))
        times_data.setncatts({u'class': u'time', u'units': u'seconds'})
        times_data[:] = times[:]
        
        # now copy the data
        for var in args.vars:
            vals = efitin[var]
            # create the secondary dimension if needed
            if vals.ndim>1:
                ldim = vals.shape[1]
                dimname = vals.dimensions[1]
                efitout.createDimension(dimname,ldim)
            newvar = efitout.createVariable(var, 'f8', vals.dimensions)
            newvar.setncatts({u'class': u'raw data', u'title': var})
            newvar[:] = vals[:]
        efitout.close()
        
        if args.delete_old:
            os.remove(infile)

if __name__ == '__main__':
    args = __parse_args()
    
    # run main
    extract_and_save(args)
