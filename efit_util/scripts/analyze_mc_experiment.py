#!/usr/bin/env python

import netCDF4, numpy as np, sys, os
import matplotlib.pyplot as plt
import efit_util as eu
import argparse

def __parse_args():
    # extract the arguments
    parser = argparse.ArgumentParser(description = 'Create plots of the MC experiment.')

    parser.add_argument('path', type = str, 
        help='master directory where the experiment is stored')
        
    parser.add_argument('--vars', type=str, nargs='+',
        help='a list of variables of interest to be processed defined by their exact path\
            in the efitOut.nc file, e.g. /output/globalParameters/q95 for q95',
        default=[])
        
    parser.add_argument('--ci', type=float, default=0.95,
        help='the confidence interval parameter in range [0, 1)')
        
    parser.add_argument('--nsigmas', type=int, default=2,
        help='number of sigmas for variance bands')

    parser.add_argument('--no-bounds', dest='no_bounds', action='store_true',
        help='by default, boundary is always examined, this flag will turn this off')
        
    parser.add_argument('--q0', action='store_true',
        help='examine q0 (shorthand for --vars /output/globalParameters/q0')
        
    parser.add_argument('--q95', action='store_true',
        help='examine q95')

    parser.add_argument('--qmin', action='store_true',
        help='examine qMin')
    
    parser.add_argument('--no-show', action='store_true', dest='no_show',
        help='dont show the plots')
    
    parser.add_argument('--save-output', dest='savepath', type=str,
        default='', help='where output (figures and output data in json format) should be stored')

    parser.add_argument('--itime', type=int, nargs='+', default=[], 
        help='by a list of integers, specify if you only want to analyze a specific timeslice(s)')
        
    parser.add_argument('--plot-limiter', action='store_true', dest='plot_limiter',
        help='plot limiter in the boundary overlay plots')
        
    args = parser.parse_args()
    
    return args
    
# what are we interested in?
def create_var_list(args):
    """ create the list of variable adresses for the  eu.utils.load_experiments(experiment_path function """
    var_list = []
    for var_string in args.vars:
        var_list.append(var_string)
        
    # add boundary - always by default, unless --no-bound given
    var_string = '/output/separatrixGeometry/boundaryCoords'
    if not var_string in var_list and not args.no_bounds:
        var_list.append(var_string)
    
    # add q0 if required
    var_string = '/output/globalParameters/q0'
    if not var_string in var_list and args.q0:
        var_list.append(var_string)
        
    # add q95
    var_string = '/output/globalParameters/q95'
    if not var_string in var_list and args.q95:
        var_list.append(var_string)

    # add qmin
    var_string = '/output/globalParameters/qMin'
    if not var_string in var_list and args.qmin:
        var_list.append(var_string)

        
    return var_list

def process_time_vec(args, time_vec):
    """ this will give a proper array of times specified by user input and available time values """
    itimes = args.itime
    if itimes == []:
        return time_vec
    else:
        Nt = len(time_vec)
        _itimes = np.array(itimes)
        _itimes -= 1
        _itimes = _itimes[_itimes<Nt]
        return (np.array(time_vec)[_itimes]).tolist()

def analyze_mc_experiment(args):
    """ run the analysis of the MC experiment """
    ci = eu.utils.confidence_interval
    c = args.ci
    nsigmas = args.nsigmas
    experiment_path = args.path

    # create the savepath
    savepath = args.savepath
    if savepath !='':
        if not os.path.exists(savepath):
            os.makedirs(savepath)

    # error magnitude
    # TODO - replace this with saving and loading some structure where this is saved
    # relerr = float(os.path.abspath(experiment_path).split('-')[-1])

    # list dirs in experiment
    dirs = os.listdir(experiment_path)
        
    # get the number of the pulse and vector of times
    pulse_number, tvec, limiter = eu.utils.load_experiment_info(experiment_path)
    var_list = create_var_list(args)
    
    # now iterate over time slices
    info ={}
    info['pulse_number'] = pulse_number
    results = []

    # check if user only wants to select somu subslices
    tvec = process_time_vec(args, tvec)

    for (idata, t) in enumerate(tvec): 
        # time
        info['t'] = t

        # print info
        print("processing timeslice t = {}".format(t))

        # now get the required data
        data = eu.utils.load_experiments(experiment_path, var_list, idata) 

        # create boxplots
        res = eu.utils.scalar_plots(data, info, c, nsigmas, savepath = savepath)

        if not args.no_bounds:
            # plot the overlayed boundaries
            if args.plot_limiter:
                eu.utils.bounds_overlay(data, info, savepath=savepath, limiter=limiter)
            else:
                eu.utils.bounds_overlay(data, info, savepath=savepath)

            # show the boundary confidence plots
            res_bounds = eu.utils.bounds_confidence_plot(data, info, c, savepath=savepath)
            
            # merge the two dictionaries
            res = {**res, **res_bounds}

            # show the boundary confidence plots
            eu.utils.bounds_sigma_plot(data, info, nsigmas, savepath=savepath)

        # save the result to json and print a savefig message
        if savepath != '':
            eu.utils.save_json(res, os.path.join(savepath, "t-{}_results.json".format(info['t'])))
            print('Plots and json output saved to '+savepath)

        # show the plots
        if not args.no_show:
            plt.show()
        
        # finally, append the res to results
        results.append(res)

    return results

if __name__ == "__main__":
    args = __parse_args()
    
    # call the main function
    results = analyze_mc_experiment(args)
