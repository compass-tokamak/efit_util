#!/usr/bin/env python

import os, sys, efit_util as eu, netCDF4, subprocess
import argparse

def __parse_args():
    # extract the arguments
    parser = argparse.ArgumentParser(description = 'Run an MC experiment of EFIT runs by adding noise to measurements.')

    parser.add_argument('inpath', 
        help='directory containing an EFIT++ run')

    parser.add_argument('runpath', 
        help='desired output directory')

    parser.add_argument('--type', type = str,
        dest = 'measurement_type', 
        help='type of signals extracted from the original efitOut.nc file to be used as input to the new run', 
        choices = ['target', 'computed'],
        default = 'computed')
        
    parser.add_argument('niter', type = int,
        help = 'number of experiment realizations')
        
    parser.add_argument('-v', action = 'store_true',
        help = 'verbose run of EFIT++')
        
    parser.add_argument('--tslices', type = float,
        dest='tslices',
        nargs="+",
        help='specify the timeslices that you want to use',
        default=None)

    parser.add_argument('--recompute-response', action = 'store_true',
        help = 'recomputes the response functions')
        
    parser.add_argument('--nc-magnetics', action = 'store_true',
        dest='nc_magnetics',
        help='store magnetic signals in .nc instead of .xml (broken due to IDAM changes)')
        
    parser.add_argument('--relerr', type = float,
        help = 'relative magnitude of error added to all inputs',
        default = None)

    parser.add_argument('--ip-relerr', dest = 'ip_relerr', type = float,
        help='relative magnitude of error added to ip input signal',
        default=None)
        
    parser.add_argument('--diamagnetic-flux-relerr', 
        dest = 'diamagnetic_flux_relerr', type = float,
        help='relative magnitude of error added to diamagnetic flux input signal',
        default=None)
        
    parser.add_argument('--mag-probes-relerr', 
        dest = 'mag_probes_relerr', type = float,
        help='relative magnitude of error added to magnetic probes input signal',
        default=None)
        
    parser.add_argument('--flux-loops-relerr', 
        dest = 'flux_loops_relerr', type = float,
        help='relative magnitude of error added to flux loops input signal',
        default=None)
        
    parser.add_argument('--pf-supplies-relerr', 
        dest = 'pf_supplies_relerr', type = float,
        help='relative magnitude of error added to pf supplies input signal',
        default=None)

    parser.add_argument('--faraday-relerr', 
        dest = 'faraday_relerr', type = float,
        help='relative magnitude of error added to faraday input signal',
        default=None)

    parser.add_argument('--lid-relerr', 
        dest = 'lid_relerr', type = float,
        help='relative magnitude of error added to lid input signal',
        default=None)

    parser.add_argument('--mse-relerr', 
        dest = 'mse_relerr', type = float,
        help='relative magnitude of error added to MSE input signal',
        default=None)

    parser.add_argument('--pressure-relerr', 
        dest = 'pressure_relerr', type = float,
        help='relative magnitude of error added to pressure input signal',
        default=None)
        
    parser.add_argument('--xml-parse-err', action = 'store_true',
        dest = 'xml_parse_verb_err',
        help = 'print xml aprsing errors')
    
    args = parser.parse_args()

    return args
    
def mc_experiment(args):
    # this will decide between abacus and freia
    host = subprocess.check_output(["echo $HOSTNAME"], shell=True).rstrip().decode()
    if host == 'abacus.tok.ipp.cas.cz':
        init_cmd = "module load efit++"
        idam_path = None
    else:
        init_cmd = "source efit++-setup efit_uda" # this is true for freia
        idam_path = None # this is true for freia

    # edit the experiment path
    experiment_path = os.path.join(args.runpath,
        '{}_N-{}'.format(args.measurement_type, args.niter, args.relerr))
    
    relerr_list = [
            'relerr', 'ip_relerr', 'diamagnetic_flux_relerr',
            'mag_probes_relerr', 'pf_supplies_relerr',
            'flux_loops_relerr', 'faraday_relerr',
            'lid_relerr', 'mse_relerr',
            'pressure_relerr'
        ]
    for relerr in relerr_list:
        if args.__contains__(relerr):
            relerr_val = args.__getattribute__(relerr)
            if relerr_val is not None:
                experiment_path += "_{}-{}".format(relerr, relerr_val)
        
    # now loop over number of repetitions
    for rep in range(1,args.niter+1):
        run_path = os.path.join(experiment_path,'{}'.format(rep))
        efitObj = eu.experiment(run_path, args.inpath, init_cmd, 
            sig_type=args.measurement_type, idam_path = idam_path,
            verb=args.v, tslices=args.tslices,
            xml_magnetics = not args.nc_magnetics,
            recompute_response = args.recompute_response,
            xml_parse_verb_err = args.xml_parse_verb_err,
            relerr=args.relerr, ip_relstd=args.ip_relerr,
            diamagnetic_flux_relstd=args.diamagnetic_flux_relerr,
            mag_probes_relstd=args.mag_probes_relerr,
            flux_loops_relstd=args.flux_loops_relerr,
            pf_supplies_relstd=args.pf_supplies_relerr,
            faraday_relstd=args.faraday_relerr,
            lid_relstd=args.lid_relerr,
            mse_relstd=args.mse_relerr,
            pressure_relstd=args.pressure_relerr
            )
            
        print('Finished run {}/{}.'.format(rep,args.niter))
        
    return True

if __name__ == '__main__':
    args = __parse_args()
    
    # run the experiment
    mc_experiment(args)
