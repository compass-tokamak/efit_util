import numpy as np
import os
import sys
import netCDF4
from lxml import etree as ET
import xmltodict
import itertools
import datetime
import tempfile
import subprocess
import shutil
import warnings
import math

class EfitIO(object):
    """ Class for creating EFIT++ inputs from efitOutput.nc file and a 
    bunch of xml input template files """
    
    XMLS = {
            'numericalControls': 'numericalControls.xml',
            'current': 'current.xml',
            'debug': 'debug.xml',
            'boundary': 'boundary.xml',
            'discdens': 'discdens.xml',
            'efitOptions_subfl': 'efitOptions_subfl.xml',
            'outputOptions': 'outputOptions.xml',
            'efitOptions_mse': 'efitOptions_mse.xml',
            'efitOptions_submp': 'efitOptions_submp.xml',
            'efitOptions_submse': 'efitOptions_submse.xml',
            'efitOptions_pf': 'efitOptions_pf.xml',
            'efitOptions_magnetics': 'efitOptions_magnetics.xml',
            'pfSystems': 'pfSystems.xml',
            'efitOptions_subpf': 'efitOptions_subpf.xml',
            'ppbasis': 'ppbasis.xml',
            'efitOptions': 'efitOptions.xml',
            'pressure': 'pressure.xml',
            'faraday': 'faraday.xml',
            'relationaldensities': 'relationaldensities.xml',
            'ffbasis': 'ffbasis.xml',
            'relationalffprime': 'relationalffprime.xml',
            'grid': 'grid.xml',
            'relationalpprime': 'relationalpprime.xml',
            'ironBoundaries': 'ironBoundaries.xml',
            'ironBoundary': 'ironBoundary.xml',
            'relationalrotationalpprime': 'relationalrotationalpprime.xml',
            'iron': 'iron.xml',
            'lid': 'lid.xml',
            'times': 'times.xml',
            'limiter': 'limiter.xml',
            'tokamakDataSource': 'tokamakDataSource.xml',
            'magnetics': 'magnetics.xml',
            'tokamakData': 'tokamakData.xml',
            'mse': 'mse.xml',
            'wwbasis': 'wwbasis.xml',
            'nebasis': 'nebasis.xml'
    }

    def __init__(self, run_path, xml_paths, rf_path=None, xml_parse_verb_err=False):
        """ 

           run_path - (non)existing folder where inputs will be stored and EFIT++ run
           xml_paths - dict of xml input names and paths, see XMLS
           rf_path - optional, points to the responseFunctions.nc file

        """
        self.set_run_path(run_path)
        self.rf_path = rf_path

        # signal labels in input nc
        self.mag_probes_label = ['timeTrace', '@signalName']
        self.flux_loops_label = ['timeTrace', '@signalName']

        # includes
        # which xmls are included in other ones? these must be 
        # parsed without the first xml line
        self.includes = []
        self.parse_xmls(xml_paths, verb_err=xml_parse_verb_err)
                            
        #self._set_xml_files(self.input_file)

        #self.record_number = 0
        #self.efit_options = efit_options
        
        # SIGNALS
        # first is values and then times for masked array usage in nc files
        self.time = None
        self.ip = None, None
        self.RB0 = None, None
        self.diamagnetic_flux = None, None
        self.mag_probes_data = None, None
        self.flux_loops_data = None, None
        self.pf_supplies_data = None, None
        # these are in the efitOut.nc file as well as in the xmls
        self.faraday_data = None, None, None
        self.boundary_data = None, None, None
        self.lid_data = None, None, None
        self.mse_data = None, None, None
        self.pressure_data = None, None, None

        # misc
        self.run_msg = ""        
        
    @classmethod
    def frompath(cls, run_path, input_path, xml_parse_verb_err=False):
        """ initialize an instance with run_path and xmls stored in input_path """
        xls = os.listdir(input_path)
        xls = list(filter(lambda x: x.find(".xml") != -1, xls))
        xd = {}
        for x in xls:
            xd[x.split(".")[0]] = x
        for name,path in xd.items():
            xd[name] = os.path.join(input_path, path)
        # also, check for existence of responseFunctions.nc file
        rfp = os.path.join(input_path, 'responseFunctions.nc')
        # response functions can be stored in a link to a file
        if os.path.isfile(rfp) | os.path.islink(rfp):
            return cls(run_path,xd,rfp,xml_parse_verb_err=xml_parse_verb_err)
        else:
            return cls(run_path,xd,xml_parse_verb_err=xml_parse_verb_err)
    
    ########################
    ### xml manipulation ###
    ########################
    def parse_xmls(self, xml_paths, verb_err=False):
        """ load xml files into internal dictionaries """
        for name in self.XMLS.keys():
            if xml_paths.get(name):
                fname = xml_paths[name]
                try: # check for empty/nonexisting files
                    with open(fname, 'r') as xml:
                        s = xml.read()
                        # in some xml files, the <Top> tag may be missing 
                        if (name in ['efitOptions', 'tokamakData', 'numericalControls',\
                           'efitOptions_magnetics', 'magnetics', 'pfSystems']) \
                           and (s.find('Top') == -1):
                            s = '<Top>'+s+'</Top>'
                        setattr(self, name, xmltodict.parse(s))
                except Exception as e:
                    print("File " + fname + " could not be read")
                    if verb_err:
                        print(e)
                    if os.path.isfile(fname):
                        setattr(self, name, None)
                    else:
                        pass
                    # this happens with empty or nonexisting .xml files - do they have to be copied anyway?
                    # yes, at least they have to be created
                 
        # set includes   
        if self.efitOptions['Top'].get('include', False):
            for inc in self.efitOptions['Top']['include']:
                self.includes.append(str(inc['@file']).split('.')[0])
        
        if self.tokamakData['Top'].get('include', False):
            for inc in self.tokamakData['Top']['include']:
                self.includes.append(str(inc['@file']).split('.')[0])
                
        if self.numericalControls['Top']['numericalControls'].get('include', False):
            for inc in self.numericalControls['Top']['numericalControls']['include']:
                self.includes.append(str(inc['@file']).split('.')[0])
                           
        # also, set the correct signal labels if they are not unique
        self._set_mag_probes_label()
        self._set_flux_loops_label()
            
    
    ##################################
    ### signal setters and getters ###
    ##################################   
    def set_time(self, times):
        self.time = np.array(times)
    
    def get_time(self):
        return self.time
        
    def set_ip(self, values, times):
        self.ip = np.array(values), np.array(times)
        
    def get_ip(self):
        return self.ip
        
    def set_RB0(self, values, times):
        self.RB0 = np.array(values), np.array(times)
        
    def get_RB0(self):
        return self.RB0    
        
    def set_diamagnetic_flux(self, values, times):
        self.diamagnetic_flux = np.array(values), np.array(times)
        
    def get_diamagnetic_flux(self):
        return self.diamagnetic_flux
        
    def set_mag_probes_data(self, values, times):
        self.mag_probes_data = np.array(values), np.array(times)    
        
    def get_mag_probes_data(self):
        return self.mag_probes_data
        
    def set_flux_loops_data(self, values, times):
        self.flux_loops_data = np.array(values), np.array(times)    
        
    def get_flux_loops_data(self):
        return self.flux_loops_data 
        
    def set_pf_supplies_data(self, values, times):
        self.pf_supplies_data = np.array(values), np.array(times)    
        
    def get_pf_supplies_data(self):
        return self.pf_supplies_data

    def set_faraday_data(self, values, times, ids):
        self.faraday_data = np.array(values), np.array(times), np.array(ids)
        
    def get_faraday_data(self):
        return self.faraday_data

    def set_boundary_data(self, values, times, ids):
        self.boundary_data = np.array(values), np.array(times), np.array(ids)
        
    def get_boundary_data(self):
        return self.boundary_data

    def set_lid_data(self, values, times, ids):
        self.lid_data = np.array(values), np.array(times), np.array(ids)
        
    def get_lid_data(self):
        return self.lid_data

    def set_mse_data(self, values, times, ids):
        self.mse_data = np.array(values), np.array(times), np.array(ids)
        
    def get_mse_data(self):
        return self.mse_data

    def set_pressure_data(self, values, times, ids):
        self.pressure_data = np.array(values), np.array(times), np.array(ids)
        
    def get_pressure_data(self):
        return self.pressure_data


    ###########################################    
    ######## other setters and getters ########
    ###########################################

    def set_run_path(self,run_path):
        """ update the run path and the path of the nc files """
        self.run_path = os.path.abspath(run_path)
        self.output_nc = os.path.join(run_path, 'efitOut.nc')
        self.input_nc = os.path.join(run_path, 'input.nc')
            
    def _subsample_signal(self, signalname, inds):
        signal = getattr(self, signalname)
        # do nothing if the signal is not set
        try:
            if signal[0] == None:
                return
        except:
            None
        # else subsample the signal values    
        signal = tuple([x[inds] for x in signal])
        setattr(self, signalname, signal)
    
    def limit_time_slices(self, tslices):
        """ limit the data only to certain timeslices """
        time = self.get_time()
        new_inds = np.array([x in tslices for x in time])
        if not(any(new_inds)):
            raise ValueError("None of the specified timeslices found in the original data, aborting.")
            
        # now that we have the indices, we can subsample the original data
        self.set_time(time[new_inds] )
        for signalname in [ "ip", "RB0", "diamagnetic_flux", "mag_probes_data", \
            "flux_loops_data", "pf_supplies_data", "faraday_data", "boundary_data",\
            "lid_data", "mse_data"]:
            self._subsample_signal(signalname, new_inds)       
        
    #####################################    
    ######## signal manipulation ########
    #####################################

    def _add_noise(self, values, relstd, absstd):
        # TODO - what is to be done if the signal does not exist? (this gets None, None)
        s = values.shape
        if relstd:
            # there is some really strange np.abs behaviour that causes the random to think 
            # that y is negative when abs is taken
            y = relstd*abs(values)
            y[np.isnan(y)] = 0
            values += np.random.normal(0.0, y, s)    
        elif absstd:
            values += np.random.normal(0.0, absstd, s) 
        return values

    def add_mag_probes_noise(self, relstd=None, absstd=None):
        """ adds noise to mag probes signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (mag probes).")
            return
        values, times = self.get_mag_probes_data()
        values = self._add_noise(values, relstd, absstd)
        self.set_mag_probes_data(values, times)   

    def add_ip_noise(self, relstd=None, absstd=None):
        """ adds noise to ip signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (ip).")
            return
        values, times = self.get_ip()
        values = self._add_noise(values, relstd, absstd)
        self.set_ip(values, times)  

    def add_diamagnetic_flux_noise(self, relstd=None, absstd=None):
        """ adds noise to diamagnetic flux signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (diamagnetic  flux).")
            return
        values, times = self.get_diamagnetic_flux()
        values = self._add_noise(values, relstd, absstd)
        self.set_diamagnetic_flux(values, times)  

    def add_flux_loops_noise(self, relstd=None, absstd=None):
        """ adds noise to flux loops signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (flux loops).")
        return
        values, times = self.get_flux_loops_data()
        values = self._add_noise(values, relstd, absstd)
        self.set_flux_loops_data(values, times)  

    def add_pf_supplies_noise(self, relstd=None, absstd=None):
        """ adds noise to pf_supplies signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (pf supplies).")
            return
        values, times = self.get_pf_supplies_data()
        values = self._add_noise(values, relstd, absstd)
        self.set_pf_supplies_data(values, times)  

    def add_faraday_noise(self, relstd=None, absstd=None):
        """ adds noise to faraday signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (faraday).")
            return

        values, times, ids = self.get_faraday_data()
        if values is not None:
            values = self._add_noise(values, relstd, absstd)
            self.set_faraday_data(values, times, ids)  

    def add_lid_noise(self, relstd=None, absstd=None):
        """ adds noise to lid signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (lid).")
            return 

        values, times, ids = self.get_lid_data()
        if values is not None:
            values = self._add_noise(values, relstd, absstd)
            self.set_lid_data(values, times, ids)  

    def add_mse_noise(self, relstd=None, absstd=None):
        """ adds noise to MSE signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (MSE).")
            return 

        values, times, ids = self.get_mse_data()
        if values is not None:
            values = self._add_noise(values, relstd, absstd)
            self.set_mse_data(values, times, ids)  

    def add_pressure_noise(self, relstd=None, absstd=None):
        """ adds noise to pressure signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (pressure).")
        
        values, times, ids = self.get_pressure_data()
        if values is not None:
            values = self._add_noise(values, relstd, absstd)
            self.set_pressure_data(values, times, ids)  


    def add_noise(self, ip_relstd=None, ip_absstd=None,
                        diamagnetic_flux_relstd=None, diamagnetic_flux_absstd=None,
                        mag_probes_relstd=None, mag_probes_absstd=None,
                        flux_loops_relstd=None, flux_loops_absstd=None,
                        pf_supplies_relstd=None, pf_supplies_absstd=None,
                        faraday_relstd=None, faraday_absstd=None,
                        lid_relstd=None, lid_absstd=None,
                        mse_relstd=None, mse_absstd=None,
                        pressure_relstd=None, pressure_absstd=None):
        """ adds different noise to all signals """
        
        if (ip_relstd or ip_absstd):
            self.add_ip_noise(relstd = ip_relstd, 
                              absstd = ip_absstd) 
        
        if (diamagnetic_flux_relstd or diamagnetic_flux_absstd):
            self.add_diamagnetic_flux_noise(relstd = diamagnetic_flux_relstd, 
                                            absstd = diamagnetic_flux_absstd) 
            
        if (mag_probes_relstd or mag_probes_absstd):
            self.add_mag_probes_noise(relstd = mag_probes_relstd, 
                                      absstd = mag_probes_absstd) 
                
        if (flux_loops_relstd or flux_loops_absstd):
            self.add_flux_loops_noise(relstd = flux_loops_relstd, 
                                      absstd = flux_loops_absstd) 
                
        if (pf_supplies_relstd or pf_supplies_absstd):
            self.add_pf_supplies_noise(relstd = pf_supplies_relstd, 
                                       absstd = pf_supplies_absstd) 

        if (faraday_relstd or faraday_absstd):
            self.add_faraday_noise(relstd = faraday_relstd, 
                                       absstd = faraday_absstd) 

        if (lid_relstd or lid_absstd):
            self.add_lid_noise(relstd = lid_relstd, 
                                       absstd = lid_absstd) 

        if (mse_relstd or mse_absstd):
            self.add_mse_noise(relstd = mse_relstd, 
                                       absstd = mse_absstd) 

        if (pressure_relstd or pressure_absstd):
            self.add_pressure_noise(relstd = pressure_relstd, 
                                       absstd = pressure_absstd) 

    def add_same_noise(self, relstd=None, absstd=None):
        """ adds the same noise to all signals """
        if not (relstd or absstd):
            print("Call with at least one std argument (all measurements).")

        self.add_noise(ip_relstd=relstd, ip_absstd=absstd,
                        diamagnetic_flux_relstd=relstd, diamagnetic_flux_absstd=absstd,
                        mag_probes_relstd=relstd, mag_probes_absstd=absstd,
                        flux_loops_relstd=relstd, flux_loops_absstd=absstd,
                        pf_supplies_relstd=relstd, pf_supplies_absstd=absstd,
                        faraday_relstd=relstd, faraday_absstd=absstd,
                        lid_relstd=relstd, lid_absstd=absstd,
                        mse_relstd=relstd, mse_absstd=absstd,
                        pressure_relstd=relstd, pressure_absstd=absstd)

    #####################################    
    ### structure setters and getters ###
    #####################################
#    def set_mag_probes(self, mag_probes):
 #       """ sets the structure of magnetic probes """
  #      return
        
    
    ######################    
    ### nc files manip ###
    ######################
    def read_nc_output(self, output_file, sig_type = 'target'):
        """ reads an efitOut.nc type file to load measured/computed signals """
        if not sig_type in ['target', 'computed']:
            print("measurement type argument must be one of 'target', 'computed'!")
            print("nothing done")
            return
        
        # open the efitOut.nc file and read data
        outnc = netCDF4.Dataset(output_file, 'r')
        
        # time
        times = outnc.variables["time"][:]
        self.set_time(times)
        
        # ip
        values = outnc["/input/constraints/plasmaCurrent/" + sig_type][:]
        #times = self.get_time[~values.mask] # only unmasked times are taken into account
        # but then also masked values must be ommited...
        self.set_ip(values, times)
        
        # RB0
        values = outnc["/input/bVacRadiusProduct/values"][:]
        self.set_RB0(values, times)
        
        # diamagnetic flux
        values = outnc["/input/constraints/diamagneticFlux/" + sig_type][:]
        self.set_diamagnetic_flux(values, times)
        
        # mag probes
        # is this ordering the same as in the efitOut.nc file?
        values = outnc["/input/constraints/magneticProbes/" + sig_type][:]
        self.set_mag_probes_data(values, times)
        
        # flux loops
        # is this ordering the same as in the efitOut.nc file?
        values = outnc["/input/constraints/fluxLoops/" + sig_type][:]
        self.set_flux_loops_data(values, times)
        
        # pf supplies
        values = outnc["/input/constraints/pfCircuits/" + sig_type][:]
        self.set_pf_supplies_data(values, times)
        
        # TODO - what is to be done if the signal does not exist?
        
        outnc.close()
    
    def save_nc_input(self, path=None): 
        """ saves the input.nc file using actual object data """
        if path is None:
            path = self.run_path
        filename = os.path.join(path, "input.nc")

        # if path does not exist, create it
        if not os.path.isdir(path):
            os.makedirs(path)
	
        
        innc = netCDF4.Dataset(filename, 'w', format='NETCDF4')
        attrs = {u'Conventions': u'Fusion-1.0, MAST-1.0',
                 u'class': u'raw data',
                 u'comment': u'',
                 u'date': datetime.date.today().strftime('%Y-%m-%d'),
                 u'shot': u'0',
                 u'software': u'datac-123, netcdf-4.3, hdf5-1.8.3',
                 u'title': u''}
        innc.setncatts(attrs)
        
        # first create the time dimension and variable
        times = self.get_time()
        innc.createDimension('time', len(times))
        times_data = innc.createVariable('time', 'f8', ('time', ))
        times_data.setncatts({u'class': u'time', u'units': u'seconds'})
        times_data[:] = times
        
        # general function for creating a variable and saving its values
        def save_variable(name, values, times):
            times = np.asarray(times)
            values = np.asarray(values)
            try:
                assert innc.variables['time'][:].shape[0] == times.shape[0] == values.shape[0]
                assert np.allclose(innc.variables['time'][:], times)
            except AssertionError:
                print(innc.variables['time'][:].shape[0])
                print(times.shape[0])
                print(values.shape[0])
                print(np.allclose(innc.variables['time'][:], times))
            values_data = innc.createVariable(name, 'f8', ('time', ))
            values_data.setncatts({u'class': u'raw data', u'title': name})
            values_data[:] = values
        
        # for  
        signal = ""
        try:    
            # save ip
            signal = "ip"
            values, times = self.get_ip()
            pcurr = self.magnetics['Top'].get('plasmaCurrent') # JET
            if not pcurr:
                pcurr = self.tokamakData['Top'].get('plasmaCurrent') # COMPASS
            if not pcurr:
                print("where is the plasmaCurrent?")
                name = 'ip' # it may be somewhere else
            else:
                name = pcurr['@signalName']
            save_variable(name, values, times)

            # save RB0
            signal = "RB0"
            values, times = self.get_RB0()
            bvac = self.magnetics['Top'].get('bVacRadiusProduct') # JET
            if not bvac:
                bvac = self.tokamakData['Top'].get('bVacRadiusProduct') # COMPASS
            if not bvac:
                print("where is the RB0?") # it may be somewhere else
                name = 'bvac'
            else:
                name = bvac['@signalName']
            save_variable(name, values, times) # on COMPASS, this is TF, why?

            # save diamagnetic flux
            signal = "diamagnetic flux"
            values, times = self.get_diamagnetic_flux()
            flx = self.magnetics['Top'].get('diamagneticFlux') # JET
            if not flx:
                flx = self.tokamakData['Top'].get('diamagneticFlux') # COMPASS
            if not flx:
                print("where is the diamagnetic flux?")# it may be somewhere else
                name = 'flx'
            else:
                name = flx['@signalName']
            save_variable(name, values, times) # on COMPASS, this is TF, why?
  
            # save mag probes measurements
            signal = "mag probes"
            values, times = self.get_mag_probes_data()
            i = 0
            for elem in itertools.chain(self.magnetics['Top']['magneticProbes']['magneticProbe']):
                for l in self.mag_probes_label:
                    elem = elem[l]
                name = elem
                save_variable(name, values[:, i], times)
                i = i + 1

            # save flux loops measurements
            signal = "flux loops"
            values, times = self.get_flux_loops_data()
            i = 0
            for elem in itertools.chain(self.magnetics['Top']['fluxLoops']['fluxLoop']):
                for l in self.flux_loops_label:
                    elem = elem[l]
                name = elem
                save_variable(name, values[:, i], times)
                i = i + 1

            # pf circuits
            signal = "pf circuits"
            values, times = self.get_pf_supplies_data()
            i = 0
            for elem in itertools.chain(self.pfSystems['Top']['pfSupplies']['pfSupply']):
                name = elem['current']['@signalName']
                #name = name.replace("/", "_") # TODO - this must change in xml as well
                save_variable(name, values[:,i], times)
                i = i + 1
        except Exception as e:
            print(e)
            raise ValueError("Saving to input.nc failed while saving " + signal + "/" + name)
        
        innc.close()
       
    ############################
    ### read&save xml inputs ###
    #############################

    def _str_to_float(self, s):
        try:
            return float(s)
        except:
            return np.nan

    def _str_to_array(self, s):
        y = np.array([x for x in map(self._str_to_float, s.split(' '))])
        inds = [not math.isnan(z) for z in y]
        return y[inds]

    def _load_xml_signal(self, xml_data):
        signals = []
        times = []
        ids = []
        for elem in itertools.chain(xml_data):
            signal = self._str_to_array(elem['@values']).copy()
            signals.append(signal)
            time = self._str_to_array(elem['@times'])
            times.append(time)
            ids.append(elem['@id'])

        # do this in case some of the values/times array is empty/has only one element
        lens = [x.shape for x in times]
        ulens = np.unique(lens)
        if ulens.shape[0] != 1:
            maxlen = np.max(lens)
            imax = np.argmax(lens)
            for i in range(len(signals)):
                if lens[i] != maxlen:
                    times[i] = times[imax]
                    signals[i] = np.zeros(maxlen)

        return np.array(signals), np.array(times), np.array(ids)

    def _load_xml_signal_timetrace(self, xml_data):
        signals = []
        times = []
        ids = []
        for elem in itertools.chain(xml_data):
            signal = self._str_to_array(elem['timeTrace']['@values'])
            signals.append(signal)
            time = self._str_to_array(elem['timeTrace']['@times'])
            times.append(time)
            ids.append(elem['@id'])
        return np.array(signals), np.array(times), np.array(ids)


    def read_xml_signals(self):
        # faraday rotation
        if hasattr(self, "faraday"):
            signals, times, ids = self._load_xml_signal(self.faraday["faradayRotationChannels"]['faradayRotationChannel'])
            self.set_faraday_data(signals, times, ids)

        # lid
        if hasattr(self, "lid"):
            signals, times, ids = self._load_xml_signal(self.lid["lineIntegratedDensities"]['lineIntegratedDensity'])
            self.set_lid_data(signals, times, ids)

        # pressure
        if hasattr(self, "pressure"):
            signals, times, ids = self._load_xml_signal(self.pressure["pressures"]['pressure'])
            self.set_pressure_data(signals, times, ids)

        # mse
        if hasattr(self, "mse"):
            signals, times, ids = self._load_xml_signal_timetrace(self.mse["mseChannels"]['mseChannel'])
            self.set_mse_data(signals, times, ids)

    def _array_to_str(self, x):
        s=''
        newline_counter = 0
        for y in x:
            newline_counter += 1
            s += str(y)+' '
            if newline_counter == 3:
                s += ""
                newline_counter = 0
        return s

    def _write_xml_signal(self, xml_data, signals):
        for (elem, signal) in zip(itertools.chain(xml_data), signals):
            elem['@values'] = self._array_to_str(signal)

    def _write_xml_signal_timetrace(self, xml_data, signals):
        for (elem, signal) in zip(itertools.chain(xml_data), signals):
            elem['timeTrace']['@values'] = self._array_to_str(signal)
            
    def write_xml_signals(self):
        # faraday rotation
        if hasattr(self, "faraday"):
            values, times, inds = self.get_faraday_data() 
            self._write_xml_signal(self.faraday["faradayRotationChannels"]['faradayRotationChannel'], values)

        # lid
        if hasattr(self, "lid"):
            values, times, inds = self.get_lid_data() 
            self._write_xml_signal(self.lid["lineIntegratedDensities"]['lineIntegratedDensity'], values)

        # pressure
        if hasattr(self, "pressure"):
            values, times, inds = self.get_pressure_data() 
            self._write_xml_signal(self.pressure["pressures"]['pressure'], values)

        # mse
        if hasattr(self, "mse"):
            values, times, inds = self.get_mse_data() 
            self._write_xml_signal_timetrace(self.mse["mseChannels"]['mseChannel'], values)
            
    def _write_xml_signal_time(self, xml_data, signals, times, identifier):
        # the transposition here is crucial, otherwise the loop is over rows
        for (elem, signal) in zip(itertools.chain(xml_data), signals.T):
            elem[identifier]['@values'] = self._array_to_str(signal)
            elem[identifier]['@times'] = self._array_to_str(times)
    
    def _remove_element(self, xml_data, element, identifier):
        for elem in itertools.chain(xml_data):
            del elem[identifier][element]

    def _write_xml_magnetics(self, xml, signals, times, adress1, adress2, identifier):
        # put the data in the 
        self._write_xml_signal_time(xml["Top"][adress1][adress2], signals, times, identifier)
        # also remove the old elements
        try:
            self._remove_element(xml["Top"][adress1][adress2], "@dataSource", identifier)
            self._remove_element(xml["Top"][adress1][adress2], "@signalName", identifier)
        except Exception as e:
            pass
            
    def _write_xml_tokamak_data(self, signals, times, adress):
        self.tokamakData["Top"][adress]['@values'] = self._array_to_str(signals)
        self.tokamakData["Top"][adress]['@times'] = self._array_to_str(times)
        del self.tokamakData["Top"][adress]["@dataSource"]
        del self.tokamakData["Top"][adress]["@signalName"]
    
    def write_xml_magnetic_signals(self):
        # mag probes
        values, times = self.get_mag_probes_data()
        self._write_xml_magnetics(self.magnetics, values, times, "magneticProbes", "magneticProbe", "timeTrace")
        
        # flux loops
        values, times = self.get_flux_loops_data()
        self._write_xml_magnetics(self.magnetics, values, times, "fluxLoops", "fluxLoop", "timeTrace")
        
        # pf coils
        values, times = self.get_pf_supplies_data()
        self._write_xml_magnetics(self.pfSystems, values, times, "pfSupplies", "pfSupply", "current")
        
        # bvac
        values, times = self.get_RB0()
        self._write_xml_tokamak_data(values, times, "bVacRadiusProduct")
        
        # ip
        values, times = self.get_ip()
        self._write_xml_tokamak_data(values, times, "plasmaCurrent")
        
        # diamagnetic flux
        values, times = self.get_diamagnetic_flux()
        self._write_xml_tokamak_data(values, times, "diamagneticFlux")   
        
        # times
        time = self.get_time()
        self.times["Top"]["timeVector"]["@tVals"] = self._array_to_str(time)
        
    ###################
    ### other utils ###
    ###################

    def save_response_functions(self,path=None):
        """ copies an existing responseFuntions.nc file to a new location """
        if path is None:
            path = self.run_path
        
        # if path does not exist, create it
        if not os.path.isdir(path):
            os.makedirs(path)
	
        if self.rf_path is not None:
            if not os.path.isfile(self.rf_path) | os.path.islink(self.rf_path):
                raise ValueError("given {} file does not exist!").format(self.rf_path)
            shutil.copyfile(self.rf_path, os.path.join(path, "responseFunctions.nc"))     
    
    def _set_mag_probes_label(self):
        """ set the proper mag probes label for saving to nc input file according to a unique tag """
        signames = []
        # create a list of the possibly used signal names
        for elem in itertools.chain(self.magnetics['Top']['magneticProbes']['magneticProbe']):
            for l in self.mag_probes_label:
                elem = elem[l]
            signames.append(elem)
        # now, decide whether it is unique
        if not len(signames) == len(list(set(signames))):
            # if not, use longName
            self.mag_probes_label = ['@longName']

    def _set_flux_loops_label(self):
        """ set the proper flux loops label for saving to nc input file according to a unique tag """
        signames = []
        # create a list of the possibly used signal names
        for elem in itertools.chain(self.magnetics['Top']['fluxLoops']['fluxLoop']):
            for l in self.flux_loops_label:
                elem = elem[l]
            signames.append(elem)
        # now, decide whether it is unique
        if not len(signames) == len(list(set(signames))):
            # if not, use longName
            self.flux_loops_label = ['@longName']


    def set_xml_files(self, idampath=None, ncfile=None):
        """ rewrites the paths in xml data files so that they point to the actual input.nc file """
        if ncfile is None:
            ncfile = os.path.join(self.run_path, "input.nc")
        idam_file = 'NETCDF::' + ncfile
        if not idampath is None:
            idam_file = 'IDAM::' + idampath + '/' + idam_file 
        # ip
        pcurr = self.magnetics['Top'].get('plasmaCurrent') # JET
        if not pcurr:
            pcurr = self.tokamakData['Top'].get('plasmaCurrent') # COMPASS
        if not pcurr:
            print("where is the plasmaCurrent?")
        else:
            pcurr['@dataSource'] = idam_file
        # set scaling factor to 1.0
        if pcurr.get('@scalingFactor'):
            pcurr['@scalingFactor'] = 1.0
        
        # RB0
        bvac = self.magnetics['Top'].get('bVacRadiusProduct') # JET
        if not bvac:
            bvac = self.tokamakData['Top'].get('bVacRadiusProduct') # COMPASS
        if not bvac:
            print("where is the RB0?") # it may be somewhere else
        else:
            bvac['@dataSource'] = idam_file
        # set scaling factor to 1.0
        if bvac.get('@scalingFactor'):
            bvac['@scalingFactor'] = 1.0
            
        # diamagnetic flux
        flx = self.magnetics['Top'].get('diamagneticFlux') # JET
        if not flx:
            flx = self.tokamakData['Top'].get('diamagneticFlux') # COMPASS
        if not flx:
            print("where is the diamagnetic flux?") # it may be somewhere else
            name = 'flx'
        else:
            flx['@dataSource'] = idam_file
        if flx.get('@scalingFactor'):
            flx['@scalingFactor'] = 1.0
            
        # mag probes measurements
        for elem in itertools.chain(self.magnetics['Top']['magneticProbes']['magneticProbe']):
            elem["timeTrace"]['@dataSource'] = idam_file
            # also, change the label so it is the same as in the .nc input file
            self._set_signal_name(elem,self.mag_probes_label)
            if elem['timeTrace'].get('@scalingFactor'):
                elem['timeTrace']['@scalingFactor'] = 1.0
                
                
        # flux loops measurements
        for elem in itertools.chain(self.magnetics['Top']['fluxLoops']['fluxLoop']):
            elem["timeTrace"]['@dataSource'] = idam_file
            # also, change the label so it is the same as in the .nc input file
            self._set_signal_name(elem,self.flux_loops_label)
            if elem['timeTrace'].get('@scalingFactor'):
                elem['timeTrace']['@scalingFactor'] = 1.0
    
        # pf circuits
        for elem in itertools.chain(self.pfSystems['Top']['pfSupplies']['pfSupply']):
            elem['current']['@dataSource'] = idam_file
            if elem['current'].get('@scalingFactor'):
                elem['current']['@scalingFactor'] = 1.0
    
    def _set_signal_name(self, element, label):
        """ changes the element['timeTrace']['@signalName'] so it matches the input.nc file """        
        # also, change the label so it is the same as in the .nc input file
        name = element
        for l in label:
            name = name[l]
        # in COMPASS, this should not change the signal name
        element["timeTrace"]['@signalName'] = name 
        
    def save_xmls(self, path=None):
        """ saves the modified xml files """
        if path is None:
            path = self.run_path
            
        # if path does not exist, create it
        if not os.path.isdir(path):
            os.makedirs(path)
		
        for name, filename in self.XMLS.items():
            # skip nonexisting but not the empty xmls
            attr = getattr(self, name, False)
            if attr is None:
                xml_str = ""
            elif not attr:
                continue
            else:
                xml_str = xmltodict.unparse(getattr(self, name), pretty = True)
            if name in self.includes:
                # this removes the xml header in included files
                xml_str = "\n".join(xml_str.split("\n")[1:])
            # this creates self-closing tags in including file                
            xml_lines = []
            for line in xml_str.split("\n"):
                if '><' in line:
                    line = (line[:line.find('><')] + '/>').strip()
                xml_lines.append(line)
            xml_str = "\n".join(xml_lines)                
            
            with open(os.path.join(path, filename), 'w') as xml:
                xml.write(xml_str)
                xml.write('\n')
                
    def run(self, efit_init_cmd, verb = True):
        """ 
			run EFIT++ in the run_path directory 
			
			efit_init_cmd = efit initialization command
			compass - export EFIT_ROOT=/compass/home/skvara/EFIT/trunk && module load efit++
			freia - source efit++-setup efit_uda
		"""
        #if there is any previous efitOut.nc file, it must first be deleted because it wont be overwritten
        if os.path.exists(self.output_nc):
            os.remove(self.output_nc)

        cmd = 'cd {} && {} && efit++.exe'.format(self.run_path, efit_init_cmd)
        print('running: {}'.format(cmd))
        
        try:
            sts = subprocess.check_output([cmd], shell=True).rstrip()
            self.run_msg = sts.decode()
            if verb:
                print(self.run_msg)
        except subprocess.CalledProcessError as e:
            warnings.warn("EFIT++ run not succesfull.", RuntimeWarning)
            self.run_msg = e.output
            for s in [b'\x80',b'\x8c',b'\x01']:
                self.run_msg = self.run_msg.replace(s,b'')
            self.run_msg = self.run_msg.decode()
            if verb:
                warnings.warn(self.run_msg, RuntimeWarning)

    def save_all_inputs(self, path=None, idam_path=None, xml_magnetics=False,
        recompute_response=False):
        """ Prepare and save all inputs.

            path (optional) - what is the folder to save inputs
            idam_path (optional) - what is the idam_path
            xml_magnetics (optional) - save signals into xmls instead of input.nc
            recompute_response (optional) - recompute response fucntions instead of copying
        """
        
        if recompute_response:
            self.numericalControls["Top"]["responseFunctions"]["@compute"] = "yes"
        else:
            self.save_response_functions(path)

        if idam_path is not None:
            ncfile = os.path.join(path,"input.nc")
        else:
            ncfile = None
            
        self.set_xml_files(idam_path,ncfile)
        self.write_xml_signals()
        
        if xml_magnetics:
            self.write_xml_magnetic_signals()
        else:
            self.save_nc_input(path)
        self.save_xmls(path)
        

####################################################################################
######################## end of EfiIO class definition #############################
####################################################################################

def init_read_save_run(run_path, input_path, init_cmd = 'source efit++-setup efit_uda',  
                       sig_type="target", idam_path=None, verb = True):
        # init
        efitObj = EfitIO.frompath(run_path, input_path)
        
        # read the data
        outnc = os.path.join(input_path, "efitOut.nc")
        efitObj.read_nc_output(outnc, sig_type = sig_type)
        efitObj.read_xml_signals()
        
        # save the data
        efitObj.save_all_inputs(idam_path = idam_path)

        # run efit
        efitObj.run(init_cmd, verb=verb)

        return efitObj

def experiment(run_path, input_path, init_cmd = 'source efit++-setup efit_uda',  
                       sig_type="target", idam_path=None, verb=True,
                       tslices=None, xml_magnetics=False,
                       recompute_response=False,
                       xml_parse_verb_err=False,
                       relerr=None, abserr=None,
                       ip_relstd=None, ip_absstd=None,
                       diamagnetic_flux_relstd=None, diamagnetic_flux_absstd=None,
                       mag_probes_relstd=None, mag_probes_absstd=None,
                       flux_loops_relstd=None, flux_loops_absstd=None,
                       pf_supplies_relstd=None, pf_supplies_absstd=None,
                       faraday_relstd=None, faraday_absstd=None,
                       lid_relstd=None, lid_absstd=None,
                       mse_relstd=None, mse_absstd=None,
                       pressure_relstd=None, pressure_absstd=None):
        # init
        efitObj = EfitIO.frompath(run_path, input_path, xml_parse_verb_err=xml_parse_verb_err)
        
        # read the data
        outnc = os.path.join(input_path, "efitOut.nc")
        efitObj.read_nc_output(outnc, sig_type = sig_type)
        efitObj.read_xml_signals()
        
        # subsample if needed
        if not tslices==None:
            efitObj.limit_time_slices(tslices)

        # add noise
        def if_none(specific, general):
            return specific if specific is not None else general
            
        efitObj.add_noise(
                   if_none(ip_relstd, relerr), 
                   if_none(ip_absstd, abserr),
                   if_none(diamagnetic_flux_relstd, relerr),
                   if_none(diamagnetic_flux_absstd, abserr),
                   if_none(mag_probes_relstd, relerr),
                   if_none(mag_probes_absstd, abserr),
                   if_none(flux_loops_relstd, relerr),
                   if_none(flux_loops_absstd, abserr),
                   if_none(pf_supplies_relstd, relerr),
                   if_none(pf_supplies_absstd, abserr),
                   if_none(faraday_relstd, relerr),
                   if_none(faraday_absstd, abserr),
                   if_none(lid_relstd, relerr),
                   if_none(lid_absstd, abserr),
                   if_none(mse_relstd, relerr),
                   if_none(mse_absstd, abserr),
                   if_none(pressure_relstd, relerr),
                   if_none(pressure_absstd, abserr)
                   )

        # save the data
        efitObj.save_all_inputs(idam_path = idam_path, 
            xml_magnetics = xml_magnetics,
            recompute_response = recompute_response)

        # run efit
        efitObj.run(init_cmd, verb=verb)

        return efitObj
